var content='<div id="sc-1253e5e9-1afa-4cff-b08a-f2462ce0321d" class="ui-page ui-scenario sc-1253e5e9-1afa-4cff-b08a-f2462ce0321d commentable" name="default">\
	<link type="text/css" rel="stylesheet" href="./resources/scenarios/1253e5e9-1afa-4cff-b08a-f2462ce0321d-1677101617564.css" />\
    <div class="filterDialog">\
      <span id="filterDialogImage"></span>\
      <span id="filterDialogTitle">Start Scenario Simulation</span>\
      <span id="filterDialogText">The prototype simulation will be restricted to the navigation defined in this scenario. You can disable this simulation by removing the filter in the top toolbar.</span>\
      <div id="filterDialogButtons">\
        <div id="startScenarioButton" class="scenarioButton">Simulate scenario</div>\
        <div id="openPageButton" class="scenarioButton">Go to screen</div>\
      </div>\
      <div id="filterCloseButton"></div>\
    </div>\
    <div class="scenarioShadow"></div>\
    <div id="scenarioWrapper">\
    </div>\
   	<div id="scenarioBgBox" ></div>\
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;