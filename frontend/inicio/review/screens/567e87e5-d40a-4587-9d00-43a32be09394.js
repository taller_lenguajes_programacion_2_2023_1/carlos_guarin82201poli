var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1280" deviceHeight="808">\
    <div id="t-662ab3f7-67d4-4a0d-a75d-a99b24e3ed90" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="plantilla" width="1280" height="800">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/662ab3f7-67d4-4a0d-a75d-a99b24e3ed90-1677101617564.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-567e87e5-d40a-4587-9d00-43a32be09394" class="screen growth-vertical devWeb canvas PORTRAIT firer ie-background commentable non-processed" alignment="left" name="Registro" width="1280" height="808">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/567e87e5-d40a-4587-9d00-43a32be09394-1677101617564.css" />\
      <div class="freeLayout">\
      <div id="s-Dynamic_Panel_1" class="dynamicpanel firer commentable pin vpin-center hpin-center non-processed-pin non-processed" customid="Form" datasizewidth="1366.0px" datasizeheight="800.4px" dataX="0.0" dataY="0.0" >\
        <div id="s-Panel_1" class="panel default firer commentable non-processed" customid="Content"  datasizewidth="1366.0px" datasizeheight="800.4px" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
          	<div class="layoutWrapper scrollable">\
          	  <div class="paddingLayer">\
                <div class="freeLayout">\
                <div id="s-Rectangle_1" class="rectangle manualfit firer commentable non-processed" customid="Image_color"   datasizewidth="617.0px" datasizeheight="1.0px" datasizewidthpx="617.0" datasizeheightpx="1.0" dataX="671.0" dataY="18.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_1_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_1" class="richtext manualfit firer mouseenter mouseleave click commentable non-processed" customid="Button_active"   datasizewidth="180.0px" datasizeheight="45.0px" dataX="220.0" dataY="655.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_1_0">Ingresar</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_2" class="richtext manualfit firer commentable non-processed" customid="Button_inactive"   datasizewidth="180.0px" datasizeheight="45.0px" dataX="220.0" dataY="655.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_2_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Group_1" class="group firer ie-background commentable non-processed" customid="Check" datasizewidth="242.0px" datasizeheight="18.0px" >\
                  <div id="s-Group_2" class="group firer ie-background commentable non-processed" customid="Checkbox" datasizewidth="18.0px" datasizeheight="18.0px" >\
                    <div id="s-Image_1" class="image lockV firer ie-background commentable hidden non-processed" customid="sign"   datasizewidth="10.0px" datasizeheight="9.0px" dataX="222.0" dataY="596.0" aspectRatio="0.9"   alt="image" systemName="./images/2d9cdf85-a2ee-4139-a928-e19ac3b3fd8f.svg" overlay="#3FB1EE">\
                      <div class="borderLayer">\
                      	<div class="imageViewport">\
                        	<?xml version="1.0" encoding="UTF-8"?>\
                        	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="12px" version="1.1" viewBox="0 0 15 12" width="15px">\
                        	    <!-- Generator: Sketch 49.3 (51167) - http://www.bohemiancoding.com/sketch -->\
                        	    <title>Path</title>\
                        	    <desc>Created with Sketch.</desc>\
                        	    <defs />\
                        	    <g fill="none" fill-rule="evenodd" id="s-Image_1-Page-1" stroke="none" stroke-width="1">\
                        	        <g fill="#B2B2B2" id="s-Image_1-Components" transform="translate(-714.000000, -1592.000000)">\
                        	            <g id="s-Image_1-Icons" transform="translate(603.000000, 1492.000000)">\
                        	                <path d="M111.173077,106.6 C111.057692,106.48 111,106.3 111,106.18 C111,106.06 111.057692,105.88 111.173077,105.76 L111.980769,104.92 C112.211538,104.68 112.557692,104.68 112.788462,104.92 L112.846154,104.98 L116.019231,108.52 C116.134615,108.64 116.307692,108.64 116.423077,108.52 L124.153846,100.18 L124.212115,100.18 C124.442308,99.94 124.789038,99.94 125.019231,100.18 L125.826923,101.02 C126.057692,101.26 126.057692,101.62 125.826923,101.86 L116.596154,111.82 C116.480769,111.94 116.365385,112 116.192308,112 C116.019231,112 115.903846,111.94 115.788462,111.82 L111.288462,106.78 L111.173077,106.6 Z" id="s-Image_1-Path" style="fill:#3FB1EE !important;" />\
                        	            </g>\
                        	        </g>\
                        	    </g>\
                        	</svg>\
\
                        </div>\
                      </div>\
                    </div>\
\
                    <div id="s-Rectangle_2" class="rectangle manualfit firer toggle ie-background commentable non-processed" customid="Rectangle"   datasizewidth="18.0px" datasizeheight="18.0px" datasizewidthpx="18.0" datasizeheightpx="18.0" dataX="218.0" dataY="591.0" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                        <div class="paddingLayer">\
                          <div class="content">\
                            <div class="valign">\
                              <span id="rtr-s-Rectangle_2_0"></span>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
\
                  <div id="s-Paragraph_3" class="richtext autofit firer commentable non-processed" customid="Text_1"   datasizewidth="333.7px" datasizeheight="16.0px" dataX="244.0" dataY="592.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-s-Paragraph_3_0">Estoy de acuerdon con los</span><span id="rtr-s-Paragraph_3_1"> </span><span id="rtr-s-Paragraph_3_2">T&eacute;rminos &amp; Condiciones</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Rectangle_3" class="rectangle manualfit firer commentable non-processed" customid="Inactive_5"   datasizewidth="366.0px" datasizeheight="42.0px" datasizewidthpx="366.0" datasizeheightpx="42.0" dataX="215.0" dataY="579.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_3_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_4" class="richtext autofit firer commentable hidden non-processed" customid="Warning_4"   datasizewidth="132.0px" datasizeheight="13.0px" dataX="218.0" dataY="540.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_4_0">Selecciona una categor&iacute;a</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_5" class="richtext autofit firer commentable non-processed" customid="Label_4"   datasizewidth="50.8px" datasizeheight="13.0px" dataX="218.0" dataY="511.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_5_0">Categor&iacute;a</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Category_1" class="dropdown firer focusin change focusout commentable non-processed" customid="Select"    datasizewidth="360.0px" datasizeheight="36.0px" dataX="218.0" dataY="502.0"  tabindex="-1"><div class="backgroundLayer">\
                  <div class="colorLayer"></div>\
                  <div class="imageLayer"></div>\
                </div><div class="borderLayer"><div class="paddingLayer"><div class="content icon"><div class="valign"><div class="value"></div></div></div></div></div><select id="s-Category_1-options" class="s-567e87e5-d40a-4587-9d00-43a32be09394 dropdown-options" ><option selected="selected" class="option"><br /></option>\
                <option  class="option">Arts &amp; Photography</option>\
                <option  class="option">Biographies &amp; Memoirs</option>\
                <option  class="option">Education &amp; Teaching</option>\
                <option  class="option">History</option>\
                <option  class="option">Literature &amp; Fiction</option>\
                <option  class="option">Science Fiction &amp; Fantasy</option>\
                <option  class="option">Travel</option></select></div>\
                <div id="s-Rectangle_4" class="rectangle manualfit firer commentable non-processed" customid="Inactive_4"   datasizewidth="366.0px" datasizeheight="54.0px" datasizewidthpx="366.0" datasizeheightpx="54.0" dataX="215.0" dataY="499.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_4_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_6" class="richtext autofit firer commentable hidden non-processed" customid="Warning_3"   datasizewidth="132.8px" datasizeheight="13.0px" dataX="218.0" dataY="465.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_6_0">Ingresa tu &aacute;rea de inter&eacute;s</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_7" class="richtext autofit firer commentable non-processed" customid="Label_3"   datasizewidth="79.1px" datasizeheight="13.0px" dataX="218.0" dataY="436.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_7_0">&Aacute;rea de inter&eacute;s</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Input_1" class="text firer focusin focusout keyup commentable non-processed" customid="Input_3"  datasizewidth="360.0px" datasizeheight="38.0px" dataX="218.0" dataY="427.0" ><div class="backgroundLayer">\
                  <div class="colorLayer"></div>\
                  <div class="imageLayer"></div>\
                </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
                <div id="s-Rectangle_5" class="rectangle manualfit firer commentable non-processed" customid="Inactive_3"   datasizewidth="366.0px" datasizeheight="54.0px" datasizewidthpx="366.0" datasizeheightpx="54.0" dataX="215.0" dataY="424.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_5_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_8" class="richtext autofit firer commentable hidden non-processed" customid="Warning_2"   datasizewidth="121.8px" datasizeheight="13.0px" dataX="218.0" dataY="389.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_8_0">Ingresa un email v&aacute;lido</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_9" class="richtext autofit firer commentable non-processed" customid="Label_2"   datasizewidth="30.3px" datasizeheight="13.0px" dataX="218.0" dataY="360.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_9_0">Email</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Input_2" class="text firer focusin focusout keyup commentable non-processed" customid="Input_2"  datasizewidth="360.0px" datasizeheight="36.0px" dataX="218.0" dataY="351.0" ><div class="backgroundLayer">\
                  <div class="colorLayer"></div>\
                  <div class="imageLayer"></div>\
                </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
                <div id="s-Rectangle_6" class="rectangle manualfit firer commentable non-processed" customid="Inactive_2"   datasizewidth="366.0px" datasizeheight="54.0px" datasizewidthpx="366.0" datasizeheightpx="54.0" dataX="215.0" dataY="348.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Rectangle_6_0"></span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_10" class="richtext autofit firer commentable hidden non-processed" customid="Warning_1"   datasizewidth="151.5px" datasizeheight="13.0px" dataX="218.0" dataY="312.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_10_0">Ingresa tu nombre completo</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_11" class="richtext autofit firer commentable non-processed" customid="Label_1"   datasizewidth="97.8px" datasizeheight="13.0px" dataX="218.0" dataY="283.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_11_0">Nombre completo</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Input_3" class="text firer focusin focusout keyup commentable non-processed" customid="Input_1"  datasizewidth="360.0px" datasizeheight="37.0px" dataX="218.0" dataY="273.0" ><div class="backgroundLayer">\
                  <div class="colorLayer"></div>\
                  <div class="imageLayer"></div>\
                </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
                <div id="s-Paragraph_12" class="richtext manualfit firer ie-background commentable non-processed" customid="Text"   datasizewidth="336.0px" datasizeheight="50.0px" dataX="218.0" dataY="187.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_12_0">Nuestros destinos te esperan!</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                <div id="s-Paragraph_13" class="richtext manualfit firer ie-background commentable non-processed" customid="Title"   datasizewidth="259.0px" datasizeheight="102.0px" dataX="218.0" dataY="102.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-s-Paragraph_13_0">Comienza tu pr&oacute;ximo viaje!</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
\
                <div id="s-Image_2" class="image firer ie-background commentable non-processed" customid="Image 1"   datasizewidth="882.3px" datasizeheight="931.0px" dataX="671.0" dataY="-81.0"   alt="image">\
                  <div class="borderLayer">\
                  	<div class="imageViewport">\
                  		<img src="./images/011cf78e-b412-4b25-b140-941c67538c03.png" />\
                  	</div>\
                  </div>\
                </div>\
\
                </div>\
\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Group_3" class="group firer ie-background commentable hidden non-processed" customid="Confirmation" datasizewidth="1200.0px" datasizeheight="750.0px" >\
        <div id="s-Rectangle_7" class="percentage rectangle manualfit firer commentable non-processed-percentage non-processed" customid="Rectangle_2"   datasizewidth="100.0%" datasizeheight="98.3%" datasizewidthpx="1280.0000000000007" datasizeheightpx="794.3546441495778" dataX="-0.0" dataY="0.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_7_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_8" class="rectangle manualfit firer commentable non-processed" customid="Rectangle_3"   datasizewidth="630.0px" datasizeheight="177.0px" datasizewidthpx="629.9999999999995" datasizeheightpx="176.96019300361877" dataX="285.0" dataY="353.9" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_8_0">Tu informaci&oacute;n se ha guardado correctamente!<br /></span><span id="rtr-s-Rectangle_8_1">&iexcl;Nos alegra tenerte con nosotros! En breve recibir&aacute; un correo electr&oacute;nico nuestro.</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_14" class="richtext manualfit firer mouseenter mouseleave click commentable non-processed" customid="Button_gotIt"   datasizewidth="180.0px" datasizeheight="44.2px" dataX="510.0" dataY="456.2" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_14_0">Entiendo</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;