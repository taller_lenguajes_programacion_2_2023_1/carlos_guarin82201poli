var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1440" deviceHeight="900">\
    <div id="t-662ab3f7-67d4-4a0d-a75d-a99b24e3ed90" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="plantilla" width="1280" height="800">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/662ab3f7-67d4-4a0d-a75d-a99b24e3ed90-1677101617564.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-7ab51cc7-2f32-40b2-afc2-c986938d6ed6" class="screen growth-vertical devWeb canvas PORTRAIT firer commentable non-processed" alignment="left" name="Login" width="1440" height="900">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/7ab51cc7-2f32-40b2-afc2-c986938d6ed6-1677101617564.css" />\
      <div class="freeLayout">\
      <div id="s-Group_5" class="group firer ie-background commentable non-processed" customid="Base popup" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Rectangle_3" class="rectangle manualfit firer commentable non-processed" customid="Base"   datasizewidth="1287.0px" datasizeheight="789.0px" datasizewidthpx="1287.0" datasizeheightpx="788.9999999999999" dataX="80.0" dataY="46.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_3_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Rectangle_1" class="rectangle manualfit firer commentable non-processed" customid="Picture"   datasizewidth="652.0px" datasizeheight="789.0px" datasizewidthpx="652.0" datasizeheightpx="789.0000000000002" dataX="715.0" dataY="46.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Rectangle_1_0"></span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="s-Polygon_1" class="path firer commentable non-processed" customid="X"   datasizewidth="14.0px" datasizeheight="14.0px" dataX="1338.0" dataY="64.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="14.0" height="14.0" viewBox="1338.0 64.0 14.0 14.0" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Polygon_1-7ab51" d="M1352.0 65.4099999666214 L1350.590000152588 64.0 L1345.0 69.59000015258789 L1339.4099999666214 64.0 L1338.0 65.4099999666214 L1343.590000152588 71.0 L1338.0 76.59000015258789 L1339.4099999666214 78.0 L1345.0 72.40999984741211 L1350.590000152588 78.0 L1352.0 76.59000015258789 L1346.409999847412 71.0 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Polygon_1-7ab51" fill="#000000" fill-opacity="1.0" opacity="0.4"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Image_1" class="image firer ie-background commentable non-processed" customid="Image 1"   datasizewidth="872.0px" datasizeheight="789.0px" dataX="715.0" dataY="46.0"   alt="image">\
          <div class="borderLayer">\
          	<div class="imageViewport">\
          		<img src="./images/42c0c3f4-9a41-4774-b385-c7cd071ded9d.png" />\
          	</div>\
          </div>\
        </div>\
\
      </div>\
\
\
      <div id="s-Group_12" class="group firer ie-background commentable non-processed" customid="Logo" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Path_1" class="path firer commentable non-processed" customid="symbol"   datasizewidth="24.8px" datasizeheight="28.6px" dataX="119.0" dataY="82.0"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="24.799999237060547" height="28.644962310791016" viewBox="119.0 82.0 24.799999237060547 28.644962310791016" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="s-Path_1-7ab51" d="M131.39999981347694 82.0 L119.0 89.1661288270724 L119.0 103.48860944722743 L131.39999981347694 110.64496124031008 L143.8 103.48860944722743 L143.8 89.1661288270724 L131.39999981347694 82.0 Z M134.41198746643317 96.32248062015503 L140.78801234704378 92.63676368973162 L140.78801234704378 99.99842126247167 L134.41198746643317 96.32248062015503 Z M139.28201926665784 90.03623114858705 L131.39999981347694 94.5822744853598 L123.51798110638825 90.03623114858705 L131.39999981347694 85.4804115237075 L139.28201926665784 90.03623114858705 Z M128.38801216052073 96.32248062015503 L122.01198727991013 100.00819717763696 L122.01198727991013 92.64653960489693 L128.38801216052073 96.32248062015503 Z M123.51798110638825 102.60873009172303 L131.39999981347694 98.05291046684349 L139.28201852056566 102.60873009172303 L131.39999981347694 107.16454897071961 L123.51798110638825 102.60873009172303 Z "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_1-7ab51" fill="#1877F2" fill-opacity="1.0"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="s-Paragraph_8" class="richtext manualfit firer ie-background commentable non-processed" customid="Login your account"   datasizewidth="122.5px" datasizeheight="50.0px" dataX="155.0" dataY="85.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_8_0">Agencia de viajes</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_14" class="group firer ie-background commentable non-processed" customid="Form" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Paragraph_2" class="richtext autofit firer click ie-background commentable non-processed" customid="Have an account"   datasizewidth="158.3px" datasizeheight="17.0px" dataX="114.5" dataY="670.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_2_0">No tienes cuenta? &nbsp;</span><span id="rtr-s-Paragraph_2_1">Reg&iacute;strate</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Group_16" class="group firer ie-background commentable non-processed" customid="Email Input" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Input_4" class="text firer commentable non-processed" customid="Input search"  datasizewidth="330.0px" datasizeheight="55.0px" dataX="241.0" dataY="303.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder="janedoe@gmail.com"/></div></div>  </div></div></div>\
          <div id="s-Paragraph_4" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="80.7px" datasizeheight="16.0px" dataX="264.0" dataY="313.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_4_0">Email</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_17" class="group firer ie-background commentable non-processed" customid="Password input" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Group_18" class="group firer ie-background commentable non-processed" customid="Your-password" datasizewidth="183.0px" datasizeheight="46.0px" >\
            <div id="s-Input_5" class="password firer commentable non-processed" customid="Input search"  datasizewidth="330.0px" datasizeheight="55.0px" dataX="241.0" dataY="383.0" ><div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="password"  value="password" maxlength="100"  tabindex="-1" placeholder="janedoe@gmail.com"/></div></div></div></div></div>\
            <div id="s-Path_7" class="path firer commentable non-processed" customid="Eye-icon"   datasizewidth="19.5px" datasizeheight="13.3px" dataX="528.0" dataY="406.0"  >\
              <div class="borderLayer">\
              	<div class="imageViewport">\
                	<?xml version="1.0" encoding="UTF-8"?>\
                	<svg xmlns="http://www.w3.org/2000/svg" width="19.5" height="13.2916259765625" viewBox="528.0 405.99999999999994 19.5 13.2916259765625" preserveAspectRatio="none">\
                	  <g>\
                	    <defs>\
                	      <path id="s-Path_7-7ab51" d="M537.75 406.0 C533.3181818181819 406.0 529.5334089669313 408.75579619368136 528.0 412.6458108108109 C529.5334089669313 416.535824864568 533.3181818181819 419.29162162162163 537.75 419.29162162162163 C542.1818181818182 419.29162162162163 545.9665913148358 416.535824864568 547.5 412.6458108108109 C545.9665913148358 408.75579619368136 542.1818181818182 406.0 537.75 406.0 Z M537.75 417.0763513513514 C535.3036361607637 417.0763513513514 533.3181818181819 415.0914688286309 533.3181818181819 412.6458108108109 C533.3181818181819 410.2001525113045 535.3036361607637 408.21527027027025 537.75 408.21527027027025 C540.1963632757013 408.21527027027025 542.1818181818182 410.2001525113045 542.1818181818182 412.6458108108109 C542.1818181818182 415.0914688286309 540.1963632757013 417.0763513513514 537.75 417.0763513513514 Z M537.75 409.98748648648643 C536.2786364988847 409.98748648648643 535.090909090909 411.17487148656073 535.090909090909 412.6458108108109 C535.090909090909 414.1167501350609 536.2786364988847 415.3041351351351 537.75 415.3041351351351 C539.2213635011151 415.3041351351351 540.409090909091 414.1167501350609 540.409090909091 412.6458108108109 C540.409090909091 411.17487148656073 539.2213635011151 409.98748648648643 537.75 409.98748648648643 Z "></path>\
                	    </defs>\
                	    <g style="mix-blend-mode:normal">\
                	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_7-7ab51" fill="#666666" fill-opacity="1.0"></use>\
                	    </g>\
                	  </g>\
                	</svg>\
\
                </div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Paragraph_6" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="83.7px" datasizeheight="16.0px" dataX="264.0" dataY="393.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Paragraph_6_0">Contrase&ntilde;a</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
\
        <div id="s-Group_3" class="group firer ie-background commentable non-processed" customid="Login Form" datasizewidth="0.0px" datasizeheight="0.0px" >\
          <div id="s-Group_4" class="group firer ie-background commentable non-processed" customid="Remember me / Forgot Password" datasizewidth="0.0px" datasizeheight="0.0px" >\
            <div id="s-Paragraph_9" class="richtext manualfit firer ie-background commentable non-processed" customid="Description"   datasizewidth="121.0px" datasizeheight="22.0px" dataX="278.0" dataY="477.0" >\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="paddingLayer">\
                  <div class="content">\
                    <div class="valign">\
                      <span id="rtr-s-Paragraph_9_0">Recu&eacute;rdame</span>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
            <div id="s-Input_1" class="checkbox firer commentable non-processed unchecked" customid="Input 1"  datasizewidth="19.0px" datasizeheight="19.0px" dataX="245.0" dataY="477.0"      tabindex="-1">\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
            </div>\
          </div>\
\
          <div id="s-Button_2" class="button multiline manualfit firer commentable non-processed" customid="Login"   datasizewidth="148.0px" datasizeheight="47.0px" dataX="423.0" dataY="464.5" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <div class="borderLayer">\
              <div class="paddingLayer">\
                <div class="content">\
                  <div class="valign">\
                    <span id="rtr-s-Button_2_0">Ingresar</span>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
\
      </div>\
\
      <div id="s-Paragraph_1" class="richtext manualfit firer ie-background commentable non-processed" customid="Login your account"   datasizewidth="296.5px" datasizeheight="47.0px" dataX="258.0" dataY="213.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_1_0">Iniciar sesi&oacute;n</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;