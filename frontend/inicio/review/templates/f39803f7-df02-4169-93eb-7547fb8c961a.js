var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1366" deviceHeight="768">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1366" height="768">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677101617564.css" />\
      <div class="freeLayout">\
      <div id="t-Rectangle_1" class="rectangle manualfit firer commentable hidden non-processed" customid="Rectangle_1"   datasizewidth="1024.0px" datasizeheight="715.0px" datasizewidthpx="1024.0" datasizeheightpx="715.0" dataX="0.0" dataY="53.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-t-Rectangle_1_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="t-topGorup" class="group firer ie-background commentable non-processed" customid="topGorup" datasizewidth="933.0px" datasizeheight="40.0px" >\
        <div id="t-Image_28" class="image firer ie-background commentable non-processed" customid="Image_28"   datasizewidth="22.0px" datasizeheight="22.0px" dataX="834.0" dataY="30.0"   alt="image" systemName="./images/9a706623-e4e1-47de-b116-4f2dca267b9e.svg" overlay="#434343">\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" height="24" version="1" viewBox="0 0 24 24" width="24"><path d="M23.854 23.146l-9.026-9.026c1.335-1.502 2.172-3.457 2.172-5.62 0-4.687-3.813-8.5-8.5-8.5s-8.5 3.813-8.5 8.5 3.813 8.5 8.5 8.5c2.163 0 4.118-.837 5.62-2.173l9.026 9.026c.098.098.226.147.354.147s.256-.049.354-.146c.195-.196.195-.512 0-.708zm-22.854-14.646c0-4.136 3.364-7.5 7.5-7.5s7.5 3.364 7.5 7.5-3.364 7.5-7.5 7.5-7.5-3.364-7.5-7.5z" fill="#434343" jimofill=" " /></svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="shapewrapper-t-productsCounter" customid="productsCounter" class="shapewrapper shapewrapper-t-productsCounter non-processed"   datasizewidth="18.0px" datasizeheight="18.0px" datasizewidthpx="18.0" datasizeheightpx="18.0" dataX="941.0" dataY="22.0" >\
            <div class="backgroundLayer">\
              <div class="colorLayer"></div>\
              <div class="imageLayer"></div>\
            </div>\
            <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" id="svg-t-productsCounter" class="svgContainer" style="width:100%; height:100%;">\
                <g>\
                    <g clip-path="url(#clip-t-productsCounter)">\
                            <ellipse id="t-productsCounter" class="ellipse shape non-processed-shape manualfit firer pageload commentable hidden non-processed" customid="productsCounter" cx="9.0" cy="9.0" rx="9.0" ry="9.0">\
                            </ellipse>\
                    </g>\
                </g>\
                <defs>\
                    <clipPath id="clip-t-productsCounter" class="clipPath">\
                            <ellipse cx="9.0" cy="9.0" rx="9.0" ry="9.0">\
                            </ellipse>\
                    </clipPath>\
                </defs>\
            </svg>\
            <div class="paddingLayer">\
                <div id="shapert-t-productsCounter" class="content firer" >\
                    <div class="valign">\
                        <span id="rtr-t-productsCounter_0">0</span>\
                    </div>\
                </div>\
            </div>\
        </div>\
\
        <div id="t-Image_13" class="image firer ie-background commentable non-processed" customid="Image_13"   datasizewidth="22.0px" datasizeheight="22.0px" dataX="929.0" dataY="30.0"   alt="image" systemName="./images/687e881d-7b4c-47e5-9026-89d3bc7f13de.svg" overlay="#434343">\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" height="64" id="t-Image_13-Layer_1" viewBox="0 0 64 64" width="64"><defs><style>#t-Image_13 .cls-1{fill:#434343 !important;}</style></defs><title>shop_cart</title><path class="cls-1" d="M10,53a3,3,0,1,0,3-3A3,3,0,0,0,10,53Zm4,0a1,1,0,1,1-1-1A1,1,0,0,1,14,53Z" fill="#434343" jimofill=" " /><path class="cls-1" d="M57,50a3,3,0,1,0,3,3A3,3,0,0,0,57,50Zm0,4a1,1,0,1,1,1-1A1,1,0,0,1,57,54Z" fill="#434343" jimofill=" " /><path class="cls-1" d="M5,10H9.26l8.45,27.24C13.18,38.46,10,43.64,10,47a1,1,0,0,0,1,1H59a1,1,0,0,0,0-2H12.12c0.59-2.67,3.3-6.61,7-7l40-4a1,1,0,0,0,.9-1V13a1,1,0,0,0-1-1H12L11,8.7A1,1,0,0,0,10,8H5A1,1,0,0,0,5,10Zm53,4V33.1L19.71,36.93,12.6,14H58Z" fill="#434343" jimofill=" " /></svg>\
\
            </div>\
          </div>\
        </div>\
\
\
        <div id="t-Image_1" class="image firer click ie-background commentable non-processed" customid="Image_1"   datasizewidth="22.0px" datasizeheight="22.0px" dataX="881.0" dataY="30.0"   alt="image" systemName="./images/9abfa42c-d649-4924-8c30-1bb3af8e9a1b.svg" overlay="#434343">\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" height="64" id="t-Image_1-Layer_1" viewBox="0 0 64 64" width="64"><defs><style>#t-Image_1 .cls-1{fill:#434343 !important;}</style></defs><title>user_2</title><path class="cls-1" d="M36.7,32.66a13,13,0,1,0-11.41,0A25,25,0,0,0,6,57a1,1,0,0,0,2,0,23,23,0,0,1,46,0,1,1,0,0,0,2,0A25,25,0,0,0,36.7,32.66ZM20,21A11,11,0,1,1,31,32,11,11,0,0,1,20,21Z" id="t-Image_1-user_2" fill="#434343" jimofill=" " /></svg>\
\
            </div>\
          </div>\
        </div>\
\
        <div id="t-Line_3" class="path firer ie-background commentable non-processed" customid="Line_3"   datasizewidth="3.0px" datasizeheight="33.0px" dataX="980.5" dataY="24.5"  >\
          <div class="borderLayer">\
          	<div class="imageViewport">\
            	<?xml version="1.0" encoding="UTF-8"?>\
            	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="32.0" viewBox="980.5 24.5 2.0 32.0" preserveAspectRatio="none">\
            	  <g>\
            	    <defs>\
            	      <path id="t-Line_3-f3980" d="M981.5 25.0 L981.5 56.0 "></path>\
            	    </defs>\
            	    <g style="mix-blend-mode:normal">\
            	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#t-Line_3-f3980" fill="none" stroke-width="1.0" stroke="#D9D9D9" stroke-linecap="butt"></use>\
            	    </g>\
            	  </g>\
            	</svg>\
\
            </div>\
          </div>\
        </div>\
        <div id="t-Hotspot_2" class="imagemap firer click ie-background commentable non-processed" customid="Hotspot_2"   datasizewidth="50.0px" datasizeheight="40.0px" dataX="917.0" dataY="20.0"  >\
          <div class="clickableSpot"></div>\
        </div>\
      </div>\
\
      <div id="t-sCart" class="dynamicpanel firer commentable hidden non-processed" customid="sCart" datasizewidth="651.0px" datasizeheight="454.0px" dataX="279.0" dataY="68.0" >\
        <div id="t-sCartList" class="panel default firer commentable non-processed" customid="sCartList"  datasizewidth="651.0px" datasizeheight="454.0px" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
          	<div class="layoutWrapper scrollable">\
          	  <div class="paddingLayer">\
                <table class="layout" summary="">\
                  <tr>\
                    <td class="layout vertical insertionpoint verticalalign sCartList sCart" valign="top" align="center" hSpacing="0" vSpacing="20"><div class="relativeLayoutWrapper t-Top "><div class="relativeLayoutWrapperResponsive">\
                <div id="t-Top" class="group firer ie-background commentable non-processed" customid="Top" datasizewidth="577.0px" datasizeheight="28.0px" >\
                  <div id="t-xClose_1" class="image firer click ie-background commentable non-processed" customid="xClose_1"   datasizewidth="16.0px" datasizeheight="16.0px" dataX="595.0" dataY="27.0"   alt="image" systemName="./images/9366eab0-d500-456d-893c-ea90e15628b3.svg" overlay="">\
                    <div class="borderLayer">\
                    	<div class="imageViewport">\
                      	<?xml version="1.0" encoding="UTF-8"?>\
                      	<svg preserveAspectRatio=\'none\' width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\
                      	    <!-- Generator: Sketch 47.1 (45422) - http://www.bohemiancoding.com/sketch -->\
                      	    <title>Fill 1</title>\
                      	    <desc>Created with Sketch.</desc>\
                      	    <defs></defs>\
                      	    <g id="t-xClose_1-Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                      	        <g id="t-xClose_1-Home-1024px" transform="translate(-943.000000, -92.000000)" fill="#666666">\
                      	            <g id="t-xClose_1-My-cart" transform="translate(341.000000, 67.000000)">\
                      	                <g id="t-xClose_1-Page-1" transform="translate(602.000000, 25.000000)">\
                      	                    <path d="M8,9.91877346 L13.6838035,15.6026412 C13.9400531,15.8588938 14.2807847,16 14.6431342,16 C15.0055741,16 15.3463056,15.8588938 15.6025553,15.6026412 C15.8588954,15.3462982 16,15.0055628 16,14.6432093 C16,14.2808557 15.8588049,13.9401203 15.6025553,13.6838678 L15.6025553,13.6837773 L9.91875177,8 L15.6025553,2.31622269 C15.8588049,2.05997015 15.9999095,1.71923477 15.9999095,1.35679074 C15.9999095,0.994437158 15.8588954,0.653701777 15.6026457,0.397449233 C15.3463056,0.141106237 15.0055741,0 14.6431342,0 C14.2807847,0 13.9400531,0.141196689 13.6838939,0.397449233 L8,6.08122654 L2.31610605,0.397449233 C2.05994686,0.141106237 1.71921533,0 1.35686585,0 C0.994335463,0 0.653694386,0.141106237 0.39744474,0.397358781 C0.141195093,0.653701777 9.04516931e-05,0.994437158 9.04516931e-05,1.35688119 C9.04516931e-05,1.71923477 0.141195093,2.05997015 0.39744474,2.31622269 L6.08124823,8 L0.397535191,13.6837773 C0.141195093,13.9401203 9.04516931e-05,14.2809461 0,14.6432997 C0,15.0056533 0.141104641,15.3463887 0.397354288,15.6025508 C0.653694386,15.8588938 0.994516366,16 1.3569563,16 C1.71921533,16 2.05994686,15.8588938 2.31610605,15.6026412 L8,9.91877346 Z" id="t-xClose_1-Fill-1"></path>\
                      	                </g>\
                      	            </g>\
                      	        </g>\
                      	    </g>\
                      	</svg>\
                      </div>\
                    </div>\
                  </div>\
\
                  <div id="t-Mycart_2" class="richtext manualfit firer ie-background commentable non-processed" customid="Mycart_2"   datasizewidth="99.0px" datasizeheight="28.0px" dataX="34.0" dataY="27.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-t-Mycart_2_0">Mi carrito</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                </div></div><div id="t-productListContainer" class="dynamicpanel firer ie-background commentable non-processed" customid="productListContainer" datasizewidth="575.0px" datasizeheight="101.0px" dataX="35.0" dataY="75.0" >\
                  <div id="t-Container" class="panel default firer ie-background commentable non-processed" customid="Container"  datasizewidth="575.0px" datasizeheight="101.0px" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                    	<div class="layoutWrapper scrollable">\
                    	  <div class="paddingLayer">\
                          <table class="layout" summary="">\
                            <tr>\
                              <td class="layout vertical insertionpoint verticalalign Container productListContainer" valign="top" align="left" hSpacing="0" vSpacing="0"><div id="t-productList" summary="" class="datalist firer ie-background commentable non-processed" customid="productList" initialRows="0" datamaster="OrderProducts" datasizewidth="575.0px" datasizeheight="1.0px" dataX="0.0" dataY="0.0" originalwidth="575.0px" originalheight="1.0px"  size="0">\
                            <div class="backgroundLayer">\
                              <div class="colorLayer"></div>\
                              <div class="imageLayer"></div>\
                            </div>\
                            <div class="borderLayer">\
                            	<div class="paddingLayer">\
                            	  <table  >\
                                  <thead>\
                                    <tr id="t-Header_1" class="headerrow firer ie-background non-processed" customid="">\
                                      <td class="hidden"></td>\
                                      <td id="t-Row_cell_3" class="datacell firer non-processed" customid="Row_cell_3"  datasizewidth="575.0px" datasizeheight="1.0px" dataX="0.0" dataY="0.0" originalwidth="575.0px" originalheight="1.0px" >\
                                        <div class="cellContainerChild">\
                                          <div class="backgroundLayer">\
                                            <div class="colorLayer"></div>\
                                            <div class="imageLayer"></div>\
                                          </div>\
                                          <div class="borderLayer">\
                                            <div class="layout scrollable">\
                                        	    <div class="paddingLayer">\
                                                <div class="freeLayout">\
                                                </div>\
\
                                              </div>\
                                            </div>\
                                          </div>\
                                        </div>\
                                      </td>\
                                    </tr>\
                                  </thead>\
                                  <tbody><tr><td></td></tr></tbody>\
                                </table>\
                              </div>\
                            </div>\
                          </div></td> \
                            </tr>\
                          </table>\
\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div><div class="relativeLayoutWrapper t-tilSubtotal "><div class="relativeLayoutWrapperResponsive">\
                <div id="t-tilSubtotal" class="group firer ie-background commentable non-processed" customid="tilSubtotal" datasizewidth="573.0px" datasizeheight="25.0px" >\
                  <div id="t-totalCost" class="dynamicpanel firer ie-background commentable non-processed" customid="totalCost" datasizewidth="140.0px" datasizeheight="24.0px" dataX="469.0" dataY="196.0" >\
                    <div id="t-Panel_2" class="panel default firer ie-background commentable non-processed" customid="Panel_2"  datasizewidth="140.0px" datasizeheight="24.0px" >\
                      <div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div>\
                      <div class="borderLayer">\
                      	<div class="layoutWrapper scrollable">\
                      	  <div class="paddingLayer">\
                            <div class="right ghostHLayout">\
                            <table class="layout" summary="">\
                              <tr>\
                                <td class="layout horizontal insertionpoint verticalalign Panel_2 totalCost" valign="top" align="right" hSpacing="1" vSpacing="0"><div id="t-Text_1" class="richtext autofit firer ie-background commentable non-processed" customid="Text_1"   datasizewidth="10.0px" datasizeheight="19.0px" dataX="70.0" dataY="0.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-t-Text_1_0">$</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div><div id="t-Text_2" class="richtext autofit firer ie-background commentable non-processed" customid="Text_2"   datasizewidth="59.0px" datasizeheight="19.0px" dataX="81.0" dataY="0.0" >\
                              <div class="backgroundLayer">\
                                <div class="colorLayer"></div>\
                                <div class="imageLayer"></div>\
                              </div>\
                              <div class="borderLayer">\
                                <div class="paddingLayer">\
                                  <div class="content">\
                                    <div class="valign">\
                                      <span id="rtr-t-Text_2_0">000.00</span>\
                                    </div>\
                                  </div>\
                                </div>\
                              </div>\
                            </div></td> \
                              </tr>\
                            </table>\
                            </div>\
\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                  <div id="t-Subtotal_1" class="richtext manualfit firer ie-background commentable non-processed" customid="Subtotal_1"   datasizewidth="98.0px" datasizeheight="24.0px" dataX="36.0" dataY="197.0" >\
                    <div class="backgroundLayer">\
                      <div class="colorLayer"></div>\
                      <div class="imageLayer"></div>\
                    </div>\
                    <div class="borderLayer">\
                      <div class="paddingLayer">\
                        <div class="content">\
                          <div class="valign">\
                            <span id="rtr-t-Subtotal_1_0">Subtotal</span>\
                          </div>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                </div></div><div id="t-Button_6" class="richtext manualfit firer click ie-background commentable non-processed" customid="Button_6"   datasizewidth="234.0px" datasizeheight="37.0px" dataX="205.0" dataY="241.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-t-Button_6_0">PAGAR</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div></td> \
                  </tr>\
                </table>\
\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      </div>\
\
      <!-- START DATA VIEW TEMPLATES -->\
      <script type="text/x-jquery-tmpl" id="t-productList-template">\
        <![CDATA[\
        <tr id="t-Current_row_1" customid="" class="datarow firer pageload ie-background non-processed " align="center">\
          <td class="hidden">\
            <input type="hidden" name="id" value="{{=it.id}}" />\
            <input type="hidden" name="datamaster" value="{{=it.datamaster}}" />\
            <input type="hidden" name="index" value="{{=it.index}}" />\
          </td>\
          <td id="t-Row_cell_4" class="datacell firer swipeleft swiperight ie-background non-processed" customid="Row_cell_4"  datasizewidth="575.0px" datasizeheight="100.0px" dataX="0.0" dataY="0.0" originalwidth="575.0px" originalheight="99.0px" >\
            <div class="cellContainerChild">\
              <div class="backgroundLayer">\
                <div class="colorLayer"></div>\
                <div class="imageLayer"></div>\
              </div>\
              <div class="borderLayer">\
                <div class="layout scrollable">\
            	    <div class="paddingLayer">\
                    <div class="freeLayout">\
                    <div id="t-Group_1" class="group firer ie-background commentable non-processed" customid="Group_1" datasizewidth="655.0px" datasizeheight="73.0px" >\
                      <div id="t-Input_11" class="text firer ie-background commentable non-processed" customid="Input_11"  datasizewidth="245.0px" datasizeheight="20.0px" dataX="92.0" dataY="12.0" ><div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text" name="67794fbb-79bb-47e2-b2d9-95c273dc6789" value="{{!it.userdata["67794fbb-79bb-47e2-b2d9-95c273dc6789"]}}" maxlength="100" readonly="readonly" tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
                      <div id="t-Input_12" class="text firer ie-background commentable non-processed" customid="Input_12"  datasizewidth="245.0px" datasizeheight="47.0px" dataX="93.0" dataY="38.0" ><div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text" name="1836c6e8-9e80-49e8-b116-4435848722bf" value="{{!it.userdata["1836c6e8-9e80-49e8-b116-4435848722bf"]}}" maxlength="100" readonly="readonly" tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
                      <div id="t-Input_13" class="text firer pageload ie-background commentable non-processed" customid="Input_13"  datasizewidth="74.0px" datasizeheight="20.0px" dataX="392.0" dataY="40.0" ><div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text" name="e1029159-a43a-42b8-af4c-c04ee4c4dd4d" value="{{!it.userdata["e1029159-a43a-42b8-af4c-c04ee4c4dd4d"]}}" maxlength="100" readonly="readonly" tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
                      <div id="t-copy_1" class="richtext manualfit firer ie-background commentable non-processed" customid="copy_1"   datasizewidth="13.0px" datasizeheight="25.0px" dataX="381.0" dataY="39.0" >\
                        <div class="backgroundLayer">\
                          <div class="colorLayer"></div>\
                          <div class="imageLayer"></div>\
                        </div>\
                        <div class="borderLayer">\
                          <div class="paddingLayer">\
                            <div class="content">\
                              <div class="valign">\
                                <span id="rtr-t-copy_1_0">$</span>\
                              </div>\
                            </div>\
                          </div>\
                        </div>\
                      </div>\
\
                      <div id="t-Image_7" class="image lockV firer ie-background commentable non-processed" customid="Image_7" name="7c564f00-5b04-4551-9c7e-cb6333e65e0d"  datasizewidth="71.0px" datasizeheight="71.0px" dataX="1.0" dataY="13.0" aspectRatio="1.0"   alt="image">\
                        <div class="borderLayer">\
                        	<div class="imageViewport">\
                        		<img src="{{!it.userdata["7c564f00-5b04-4551-9c7e-cb6333e65e0d"]}}" />\
                        	</div>\
                        </div>\
                      </div>\
\
                      <div id="t-Category_1" class="dropdown firer change click commentable non-processed" customid="Category_1" name="a84ba135-2ff9-4f56-85d1-c15434e35673"   datasizewidth="56.0px" datasizeheight="25.0px" dataX="518.0" dataY="37.0"  tabindex="-1"><div class="backgroundLayer">\
                        <div class="colorLayer"></div>\
                        <div class="imageLayer"></div>\
                      </div><div class="borderLayer"><div class="paddingLayer"><div class="content icon"><div class="valign"><div class="value">{{!it.userdata["a84ba135-2ff9-4f56-85d1-c15434e35673"]}}</div></div></div></div></div><select id="t-Category_1-options" class="t-f39803f7-df02-4169-93eb-7547fb8c961a dropdown-options" ><option class="option" {{? jimData.hasValue(it.userdata["a84ba135-2ff9-4f56-85d1-c15434e35673"], "1") }}selected="selected"{{?}}>1</option>\
                      <option class="option" {{? jimData.hasValue(it.userdata["a84ba135-2ff9-4f56-85d1-c15434e35673"], "2") }}selected="selected"{{?}}>2</option>\
                      <option class="option" {{? jimData.hasValue(it.userdata["a84ba135-2ff9-4f56-85d1-c15434e35673"], "3") }}selected="selected"{{?}}>3</option>\
                      <option class="option" {{? jimData.hasValue(it.userdata["a84ba135-2ff9-4f56-85d1-c15434e35673"], "4") }}selected="selected"{{?}}>4</option>\
                      <option class="option" {{? jimData.hasValue(it.userdata["a84ba135-2ff9-4f56-85d1-c15434e35673"], "5") }}selected="selected"{{?}}>5</option></select></div>\
                      <div id="t-Path_1" class="path firer ie-background commentable non-processed" customid="Line_1"   datasizewidth="3.0px" datasizeheight="50.0px" dataX="603.0" dataY="26.0"  >\
                        <div class="borderLayer">\
                        	<div class="imageViewport">\
                          	<?xml version="1.0" encoding="UTF-8"?>\
                          	<svg xmlns="http://www.w3.org/2000/svg" width="2.0" height="49.0" viewBox="603.0 26.0 2.0 49.0" preserveAspectRatio="none">\
                          	  <g>\
                          	    <defs>\
                          	      <path id="t-Path_1-f3980" d="M604.0 26.5 L604.0 74.5 "></path>\
                          	    </defs>\
                          	    <g style="mix-blend-mode:normal">\
                          	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#t-Path_1-f3980" fill="none" stroke-width="1.0" stroke="#D9D9D9" stroke-linecap="butt"></use>\
                          	    </g>\
                          	  </g>\
                          	</svg>\
\
                          </div>\
                        </div>\
                      </div>\
\
                      <div id="t-Image_11" class="image lockV firer click ie-background commentable non-processed" customid="Image_11"   datasizewidth="32.0px" datasizeheight="32.0px" dataX="624.0" dataY="30.0" aspectRatio="1.0"   alt="image" systemName="./images/b8f326c8-a66d-48e3-86b2-1c3872a315cc.svg" overlay="#A9A9A9">\
                        <div class="borderLayer">\
                        	<div class="imageViewport">\
                          	<?xml version="1.0" encoding="UTF-8"?>\
                          	<svg preserveAspectRatio=\'none\' xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" height="64" id="t-Image_11-Layer_1" viewBox="0 0 64 64" width="64"><defs><style>#t-Image_11 .cls-1{fill:#A9A9A9 !important;}</style></defs><title>trash</title><path class="cls-1" d="M12,14H50v2H12V14Zm28,1H38V12a2,2,0,0,0-2-2H26a2,2,0,0,0-2,2v3H22V12a4,4,0,0,1,4-4H36a4,4,0,0,1,4,4v3ZM25,20l1,32L24,52,23,20Zm12,0L39,20,38,52,36,52Zm-7,0h2V52H30V20ZM41,58H21a4.06,4.06,0,0,1-4-3.88l-3-39,2-.15,3,39A2.1,2.1,0,0,0,21,56H41a2.18,2.18,0,0,0,2-2.12l3-39,2,0.15-3,39A4.16,4.16,0,0,1,41,58Z" id="t-Image_11-trash" fill="#A9A9A9" jimofill=" " /></svg>\
\
                          </div>\
                        </div>\
                      </div>\
\
                    </div>\
\
                    </div>\
\
                  </div>\
                </div>\
              </div>\
            </div>\
          </td>\
        </tr>\
        ]]>\
      </script>\
      <!-- END DATA VIEW TEMPLATES -->\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;