var content='<div class="ui-page" deviceName="ipad" deviceType="mobile" deviceWidth="768" deviceHeight="1024">\
    <div id="m-b6c9522a-9815-4308-9d10-0fc53d52c801" class="master growth-both devMobile devIOS ipad-device canvas firer ie-background commentable non-processed" alignment="left" name="ProductInfo" originalWidth="768" originalHeight="1024" >\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/masters/b6c9522a-9815-4308-9d10-0fc53d52c801-1677101617564.css" />\
      <div class="freeLayout">\
      <div id="Rectangle_3" class="rectangle manualfit firer commentable non-processed" customid="Rectangle_3"   datasizewidth="343.0px" datasizeheight="262.0px" datasizewidthpx="343.0" datasizeheightpx="262.0" dataX="199.0" dataY="431.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-Rectangle_3_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="Product_Info" class="dynamicpanel firer ie-background commentable non-processed" customid="Product_Info" datasizewidth="342.0px" datasizeheight="263.0px" dataX="200.0" dataY="431.0" >\
        <div id="Description" class="panel default firer ie-background commentable non-processed" customid="Descripci&oacute;n"  datasizewidth="342.0px" datasizeheight="263.0px" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
          	<div class="layoutWrapper scrollable">\
          	  <div class="paddingLayer">\
                <div class="freeLayout">\
                <div id="CHRISSTEVENSON_1" class="richtext manualfit firer ie-background commentable non-processed" customid="CHRISSTEVENSON_1"   datasizewidth="342.0px" datasizeheight="263.0px" dataX="0.0" dataY="0.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-CHRISSTEVENSON_1_0">Tokio</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                </div>\
\
              </div>\
            </div>\
          </div>\
        </div>\
        <div id="Reviews" class="panel hidden firer ie-background commentable non-processed" customid="Rese&ntilde;as"  datasizewidth="342.0px" datasizeheight="263.0px" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
          	<div class="layoutWrapper scrollable">\
          	  <div class="paddingLayer">\
                <div class="freeLayout">\
                <div id="Paragraph_3" class="richtext manualfit firer ie-background commentable non-processed" customid="Paragraph_3"   datasizewidth="341.0px" datasizeheight="270.0px" dataX="0.0" dataY="0.0" >\
                  <div class="backgroundLayer">\
                    <div class="colorLayer"></div>\
                    <div class="imageLayer"></div>\
                  </div>\
                  <div class="borderLayer">\
                    <div class="paddingLayer">\
                      <div class="content">\
                        <div class="valign">\
                          <span id="rtr-Paragraph_3_0">A qui&eacute;n le guste lo moderno pero que no est&eacute; re&ntilde;ido con lo antiguo, le recomiendo que vaya a Tokyo y pasee, simplemente, y saborear&aacute; otro mundo. Es inolvidable.<br /><br />En este a&ntilde;o, Abril 2013, fuimos con mi amada esposa a Tokyo, fu&eacute; tambi&eacute;n un hermoso viaje, una ciudad bella, super moderna y ordenada, su gente muy culta y amable.</span>\
                        </div>\
                      </div>\
                    </div>\
                  </div>\
                </div>\
                </div>\
\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="REVIEWS" class="richtext manualfit firer click ie-background commentable non-processed" customid="RESE&Ntilde;AS"   datasizewidth="94.0px" datasizeheight="26.0px" dataX="299.0" dataY="405.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-REVIEWS_0">REVIEWS</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="DESCRIPTION" class="richtext manualfit firer click ie-background commentable non-processed" customid="DESCRIPCI&Oacute;N"   datasizewidth="100.0px" datasizeheight="23.0px" dataX="200.0" dataY="406.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-DESCRIPTION_0"></span><span id="rtr-DESCRIPTION_1">DESCRIPTION</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="Path_2" class="path firer ie-background commentable non-processed" customid="Line_2"   datasizewidth="107.0px" datasizeheight="7.0px" dataX="198.5" dataY="427.5"  >\
        <div class="borderLayer">\
        	<div class="imageViewport">\
          	<?xml version="1.0" encoding="UTF-8"?>\
          	<svg xmlns="http://www.w3.org/2000/svg" width="102.0" height="4.0" viewBox="198.5 427.5 102.0 4.0" preserveAspectRatio="none">\
          	  <g>\
          	    <defs>\
          	      <path id="m-Path_2-b6c95" d="M199.0 429.5 L300.0 429.5 "></path>\
          	    </defs>\
          	    <g style="mix-blend-mode:normal">\
          	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#m-Path_2-b6c95" fill="none" stroke-width="3.0" stroke="#7D5FFF" stroke-linecap="butt"></use>\
          	    </g>\
          	  </g>\
          	</svg>\
\
          </div>\
        </div>\
      </div>\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;