jQuery("#simulation")
  .on("click", ".s-685c25f0-2f46-4dff-a050-86f931de0df8 .click", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Image_20")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/390b9a82-14f7-4a7e-b714-3158b656b9f6",
                    "transition": {
                      "type": "fade",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_46")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_46 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_46 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_46 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_46 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_46 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_46 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Paragraph_45",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Paragraph_45",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-OverEar_Wireless_Bl",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_44",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_21")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/a3abeb3c-4193-47b4-b708-428758bd196a"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_47")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_47" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_21" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/a3abeb3c-4193-47b4-b708-428758bd196a"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-arrowGoLeft")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_2","#s-page_12","#s-page_7" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_36")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/14076d24-bea1-496b-a28b-dd4f95f3146b"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_83")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_83" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_36" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/14076d24-bea1-496b-a28b-dd4f95f3146b"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_58")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_58 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_58 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_58 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_58 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_58 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_58 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Paragraph_26",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Paragraph_14",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-Paragraph_16",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_15",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-arrowGoLeft_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_9","#s-page_8","#s-page_3" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_46")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/390b9a82-14f7-4a7e-b714-3158b656b9f6"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_110")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_110" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_46" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/390b9a82-14f7-4a7e-b714-3158b656b9f6"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_61")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_61 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_61 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_61 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_61 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_61 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_61 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Paragraph_108",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Paragraph_108",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-OverEar_Wireless_Bl",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_107",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-arrowGoLeft_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_10","#s-page_5","#s-page_4" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_56")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/a3abeb3c-4193-47b4-b708-428758bd196a"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_135")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_135" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_56" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/a3abeb3c-4193-47b4-b708-428758bd196a"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_67")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_67 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_67 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_67 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_67 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_67 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_67 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Odin",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Odin",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-OverEar_Wireless_Bl",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_4",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-arrowGoLeft_3")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_6","#s-page_1","#s-page_11" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_22")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/390b9a82-14f7-4a7e-b714-3158b656b9f6",
                    "transition": {
                      "type": "fade",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_23")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/14076d24-bea1-496b-a28b-dd4f95f3146b"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_52")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_52" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_23" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/14076d24-bea1-496b-a28b-dd4f95f3146b"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_62")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_62 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_62 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_62 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_62 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_62 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_62 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Paragraph_45",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Paragraph_45",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-OverEar_Wireless_Bl",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_44",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_37")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/390b9a82-14f7-4a7e-b714-3158b656b9f6"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_86")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_86" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_37" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/390b9a82-14f7-4a7e-b714-3158b656b9f6"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_63")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_63 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_63 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_63 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_63 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_63 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_63 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Paragraph_26",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Paragraph_14",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-Paragraph_16",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_15",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_48")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/a3abeb3c-4193-47b4-b708-428758bd196a"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_115")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_115" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_48" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/a3abeb3c-4193-47b4-b708-428758bd196a"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_65")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_65 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_65 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_65 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_65 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_65 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_65 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Paragraph_108",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Paragraph_108",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-OverEar_Wireless_Bl",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_107",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_58")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/14076d24-bea1-496b-a28b-dd4f95f3146b"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_140")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_140" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_58" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/14076d24-bea1-496b-a28b-dd4f95f3146b"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_70")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_70 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_70 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_70 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_70 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_70 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_70 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Odin",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Odin",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-OverEar_Wireless_Bl",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_4",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_25")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/390b9a82-14f7-4a7e-b714-3158b656b9f6",
                    "transition": {
                      "type": "fade",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_26")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/390b9a82-14f7-4a7e-b714-3158b656b9f6"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_59")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_59" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_26" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/390b9a82-14f7-4a7e-b714-3158b656b9f6"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_51")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_51 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_51 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_51 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_51 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_51 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_51 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Paragraph_45",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Paragraph_45",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-OverEar_Wireless_Bl",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_44",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-arrowGoRight")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_2","#s-page_12","#s-page_7" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_38")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/a3abeb3c-4193-47b4-b708-428758bd196a"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_89")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_89" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_38" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/a3abeb3c-4193-47b4-b708-428758bd196a"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_64")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_64 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_64 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_64 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_64 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_64 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_64 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Paragraph_26",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Paragraph_14",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-Paragraph_16",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_15",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-arrowGoRight_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_9","#s-page_8","#s-page_3" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_50")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/14076d24-bea1-496b-a28b-dd4f95f3146b"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_120")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_120" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_50" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/14076d24-bea1-496b-a28b-dd4f95f3146b"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_66")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_66 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_66 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_66 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_66 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_66 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_66 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Paragraph_108",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Paragraph_108",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-OverEar_Wireless_Bl",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_107",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-arrowGoRight_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_10","#s-page_5","#s-page_4" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_60")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/390b9a82-14f7-4a7e-b714-3158b656b9f6"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_145")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_145" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_60" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/390b9a82-14f7-4a7e-b714-3158b656b9f6"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_68")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_68 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_68 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_68 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_68 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_68 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_68 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Odin",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Odin",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-OverEar_Wireless_Bl",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_4",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-arrowGoRight_3")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_6","#s-page_1","#s-page_11" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-MOREINFO")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-MOREINFO" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_6" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/390b9a82-14f7-4a7e-b714-3158b656b9f6"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_6")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/390b9a82-14f7-4a7e-b714-3158b656b9f6"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Button_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_1 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_1 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_1 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_1 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_1 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_1 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Odin",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Odin",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-OverEar_Wireless_Bl",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_4",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_8")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_8" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_2" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/390b9a82-14f7-4a7e-b714-3158b656b9f6",
                    "transition": {
                      "type": "fade",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Button_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_2 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_2 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_2 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_2 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_2 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_2 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Paragraph_9",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Paragraph_9",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-Paragraph_11",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_22",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_13")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Paragraph_13" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Image_4" ],
                    "attributes": {
                      "opacity": "0.5"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Button_3")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_3 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_3 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_3 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_3 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_3 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #s-Button_3 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "SelectectProduct" ],
                    "value": {
                      "datatype": "property",
                      "target": "#s-Paragraph_14",
                      "property": "jimGetValue"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    }
                  }
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "variable",
                          "element": "SelectectProduct"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        },"1" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "actions": [
                {
                  "action": "jimCreateData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts",
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": {
                        "datatype": "property",
                        "target": "#s-Paragraph_14",
                        "property": "jimGetValue"
                      },
                      "1836c6e8-9e80-49e8-b116-4435848722bf": {
                        "datatype": "property",
                        "target": "#s-Paragraph_16",
                        "property": "jimGetValue"
                      },
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": {
                        "datatype": "property",
                        "target": "#s-Paragraph_15",
                        "property": "jimGetValue"
                      },
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": "1"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "ShowHideCart"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "timed",
          "delay": 2000
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("swipeleft", ".s-685c25f0-2f46-4dff-a050-86f931de0df8 .swipeleft", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-page_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_2","#s-page_12","#s-page_7" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_9","#s-page_8","#s-page_3" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_3")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_10","#s-page_5","#s-page_4" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_4")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_6","#s-page_1","#s-page_11" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_5")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_6","#s-page_1","#s-page_11" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_6")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_2","#s-page_12","#s-page_7" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_7")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_9","#s-page_8","#s-page_3" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_8")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_10","#s-page_5","#s-page_4" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_9")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_10","#s-page_5","#s-page_4" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_10")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_6","#s-page_1","#s-page_11" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_11")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_2","#s-page_12","#s-page_7" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_12")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_9","#s-page_8","#s-page_3" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Panel_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Panel_3" ],
                    "transition": {
                      "type": "fade",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Panel_6" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_3 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#999999"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_1 > .backgroundLayer > .colorLayer",
                  "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_2 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#D9D9D9"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Panel_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Panel_1" ],
                    "transition": {
                      "type": "fade",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Panel_4" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_1 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#999999"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_3 > .backgroundLayer > .colorLayer",
                  "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_2 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#D9D9D9"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Panel_3")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Panel_2" ],
                    "transition": {
                      "type": "fade",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Panel_5" ],
                    "transition": {
                      "type": "slideleft",
                      "duration": 700
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_2 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#999999"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_3 > .backgroundLayer > .colorLayer",
                  "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_1 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#D9D9D9"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("swiperight", ".s-685c25f0-2f46-4dff-a050-86f931de0df8 .swiperight", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-page_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_10","#s-page_5","#s-page_4" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_6","#s-page_1","#s-page_11" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_3")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_2","#s-page_12","#s-page_7" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_4")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_9","#s-page_8","#s-page_3" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_5")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_9","#s-page_8","#s-page_3" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_6")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_10","#s-page_5","#s-page_4" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_7")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_6","#s-page_1","#s-page_11" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_8")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_2","#s-page_12","#s-page_7" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_9")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_2","#s-page_12","#s-page_7" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_10")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_9","#s-page_8","#s-page_3" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_11")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_10","#s-page_5","#s-page_4" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-page_12")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-page_6","#s-page_1","#s-page_11" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-arrowGoLeft_1","#s-arrowGoLeft","#s-arrowGoRight_1","#s-arrowGoRight","#s-arrowGoRight_3","#s-arrowGoRight_2","#s-arrowGoLeft_2","#s-arrowGoLeft_3" ],
                    "effect": {
                      "type": "fade",
                      "easing": "linear",
                      "duration": 100
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Panel_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Panel_2" ],
                    "transition": {
                      "type": "fade",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Panel_5" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_2 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#999999"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_3 > .backgroundLayer > .colorLayer",
                  "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_1 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#D9D9D9"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Panel_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Panel_3" ],
                    "transition": {
                      "type": "fade",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Panel_6" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_3 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#999999"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_1 > .backgroundLayer > .colorLayer",
                  "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_2 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#D9D9D9"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Panel_3")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Panel_1" ],
                    "transition": {
                      "type": "fade",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Panel_4" ],
                    "transition": {
                      "type": "slideright",
                      "duration": 700
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_1 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#999999"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_3 > .backgroundLayer > .colorLayer",
                  "#s-685c25f0-2f46-4dff-a050-86f931de0df8 #shapewrapper-s-Ellipse_2 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#D9D9D9"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  });