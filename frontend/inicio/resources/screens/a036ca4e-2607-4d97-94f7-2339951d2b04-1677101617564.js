jQuery("#simulation")
  .on("click", ".s-a036ca4e-2607-4d97-94f7-2339951d2b04 .click", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Paragraph_17")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Paragraph_17 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Paragraph_17 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Paragraph_17 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Paragraph_17 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Paragraph_17 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Paragraph_17 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_1",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Text_2" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_1 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_1 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#FD676A",
                      "border-right-width": "1.0px",
                      "border-right-color": "#FD676A",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#FD676A",
                      "border-left-width": "1.0px",
                      "border-left-color": "#FD676A",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_1 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-FIRSTNAME .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-FIRSTNAME span" ],
                    "attributes": {
                      "color": "#FD676A",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "1"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_2",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Text_1" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_2 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_2 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#FD676A",
                      "border-right-width": "1.0px",
                      "border-right-color": "#FD676A",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#FD676A",
                      "border-left-width": "1.0px",
                      "border-left-color": "#FD676A",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_2 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-SECONDNAME .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-SECONDNAME span" ],
                    "attributes": {
                      "color": "#FD676A",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "1"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_1",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Text_3" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_3 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_3 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#FD676A",
                      "border-right-width": "1.0px",
                      "border-right-color": "#FD676A",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#FD676A",
                      "border-left-width": "1.0px",
                      "border-left-color": "#FD676A",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_3 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-EMAIL .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-EMAIL span" ],
                    "attributes": {
                      "color": "#FD676A",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "1"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_4",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Text_4" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_4 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_4 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#FD676A",
                      "border-right-width": "1.0px",
                      "border-right-color": "#FD676A",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#FD676A",
                      "border-left-width": "1.0px",
                      "border-left-color": "#FD676A",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_4 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-PHONENUMBER .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-PHONENUMBER span" ],
                    "attributes": {
                      "color": "#FD676A",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "1"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_5",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Text_5" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_5 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_5 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#FD676A",
                      "border-right-width": "1.0px",
                      "border-right-color": "#FD676A",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#FD676A",
                      "border-left-width": "1.0px",
                      "border-left-color": "#FD676A",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_5 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-ADRESS .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-ADRESS span" ],
                    "attributes": {
                      "color": "#FD676A",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "1"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_6",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Text_9" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_6 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_6 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#FD676A",
                      "border-right-width": "1.0px",
                      "border-right-color": "#FD676A",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#FD676A",
                      "border-left-width": "1.0px",
                      "border-left-color": "#FD676A",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_6 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-ZIPCODE .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-ZIPCODE span" ],
                    "attributes": {
                      "color": "#FD676A",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "1"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_7",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Text_10" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_7 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_7 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#FD676A",
                      "border-right-width": "1.0px",
                      "border-right-color": "#FD676A",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#FD676A",
                      "border-left-width": "1.0px",
                      "border-left-color": "#FD676A",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_7 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-STATE .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-STATE span" ],
                    "attributes": {
                      "color": "#FD676A",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "1"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_4",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Text_6" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_8 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_8 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#FD676A",
                      "border-right-width": "1.0px",
                      "border-right-color": "#FD676A",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#FD676A",
                      "border-left-width": "1.0px",
                      "border-left-color": "#FD676A",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_8 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-CARDNUMBER .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-CARDNUMBER span" ],
                    "attributes": {
                      "color": "#FD676A",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "1"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_9",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Text_7" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_9 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_9 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#FD676A",
                      "border-right-width": "1.0px",
                      "border-right-color": "#FD676A",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#FD676A",
                      "border-left-width": "1.0px",
                      "border-left-color": "#FD676A",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_9 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-NAMEON_CARD .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-NAMEON_CARD span" ],
                    "attributes": {
                      "color": "#FD676A",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "1"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLess",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-monthCard",
                  "property": "jimGetSelectedValue"
                },{
                  "datatype": "property",
                  "target": "#s-Text_14",
                  "property": "jimGetValue"
                } ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Text_11" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .line_options.selected" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .selected > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .line_options:hover" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .line_options:hover > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-monthCard > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "4.0px",
                      "border-top-right-radius": "4.0px",
                      "border-bottom-right-radius": "4.0px",
                      "border-bottom-left-radius": "4.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-monthCard > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#FD676A",
                      "border-right-width": "1.0px",
                      "border-right-color": "#FD676A",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#FD676A",
                      "border-left-width": "1.0px",
                      "border-left-color": "#FD676A",
                      "border-top-left-radius": "4.0px",
                      "border-top-right-radius": "4.0px",
                      "border-bottom-right-radius": "4.0px",
                      "border-bottom-left-radius": "4.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-monthCard > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "4.0px",
                      "border-top-right-radius": "4.0px",
                      "border-bottom-right-radius": "4.0px",
                      "border-bottom-left-radius": "4.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-VALIDTHROUGH .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-VALIDTHROUGH span" ],
                    "attributes": {
                      "color": "#FD676A",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "1"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLess",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-yearCard",
                  "property": "jimGetSelectedValue"
                },{
                  "datatype": "property",
                  "target": "#s-Text_12",
                  "property": "jimGetValue"
                } ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Text_11" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .line_options.selected" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .selected > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .line_options:hover" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .line_options:hover > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-monthCard > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "4.0px",
                      "border-top-right-radius": "4.0px",
                      "border-bottom-right-radius": "4.0px",
                      "border-bottom-left-radius": "4.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-monthCard > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#FD676A",
                      "border-right-width": "1.0px",
                      "border-right-color": "#FD676A",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#FD676A",
                      "border-left-width": "1.0px",
                      "border-left-color": "#FD676A",
                      "border-top-left-radius": "4.0px",
                      "border-top-right-radius": "4.0px",
                      "border-bottom-right-radius": "4.0px",
                      "border-bottom-left-radius": "4.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-monthCard > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "4.0px",
                      "border-top-right-radius": "4.0px",
                      "border-bottom-right-radius": "4.0px",
                      "border-bottom-left-radius": "4.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-VALIDTHROUGH .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-VALIDTHROUGH span" ],
                    "attributes": {
                      "color": "#FD676A",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "1"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Input_10",
                  "property": "jimGetValue"
                },"" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Text_8" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_10 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_10 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#FD676A",
                      "border-right-width": "1.0px",
                      "border-right-color": "#FD676A",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#FD676A",
                      "border-left-width": "1.0px",
                      "border-left-color": "#FD676A",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_10 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-CVCCODE .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-CVCCODE span" ],
                    "attributes": {
                      "color": "#FD676A",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "1"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "PurchaseValidation"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Confirmation" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-monthCard")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Text_11" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .line_options.selected" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .selected > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .line_options:hover" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .line_options:hover > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-monthCard > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#999999",
                      "border-right-width": "1.0px",
                      "border-right-color": "#999999",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#999999",
                      "border-left-width": "1.0px",
                      "border-left-color": "#999999"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-VALIDTHROUGH span" ],
                    "attributes": {
                      "color": "#585858",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-yearCard")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Text_11" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-yearCard .line_options.selected" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-yearCard .selected > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-yearCard .line_options:hover" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-yearCard .line_options:hover > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-yearCard > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#999999",
                      "border-right-width": "1.0px",
                      "border-right-color": "#999999",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#999999",
                      "border-left-width": "1.0px",
                      "border-left-color": "#999999"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-VALIDTHROUGH .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-VALIDTHROUGH span" ],
                    "attributes": {
                      "color": "#585858",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is(".s-Image_11")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimDeleteData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "property",
                          "target": ".s-Input_11",
                          "property": "jimGetValue"
                        } ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Text_19" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Text_20" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimPlus",
                            "parameter": [ {
                              "action": "jimMultiply",
                              "parameter": [ {
                                "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                              },{
                                "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                              } ]
                            },{
                              "datatype": "property",
                              "target": "#s-Paragraph_7",
                              "property": "jimGetValue"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  }
                },"3" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#s-Panel_3" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "datatype": "property",
                        "target": "#s-productList",
                        "property": "jimGetHeight"
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Continuebuying_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Deactivated" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "down"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Text_18" ],
                    "value": {
                      "action": "jimSubstring",
                      "parameter": [ {
                        "action": "jimSystemDate"
                      },"0","2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Text_17" ],
                    "value": {
                      "action": "jimSubstring",
                      "parameter": [ {
                        "action": "jimSystemDate"
                      },"3","5" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Text_16" ],
                    "value": {
                      "action": "jimSubstring",
                      "parameter": [ {
                        "action": "jimSystemDate"
                      },"6","10" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Text_17",
                  "property": "jimGetValue"
                },"21" ]
              },
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Get_it_by_2" ],
                    "value": {
                      "action": "jimConcat",
                      "parameter": [ {
                        "action": "jimConcat",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#s-Text_18",
                          "property": "jimGetValue"
                        },"-" ]
                      },{
                        "action": "jimConcat",
                        "parameter": [ {
                          "action": "jimConcat",
                          "parameter": [ {
                            "action": "jimPlus",
                            "parameter": [ {
                              "datatype": "property",
                              "target": "#s-Text_17",
                              "property": "jimGetValue"
                            },"8" ]
                          },"-" ]
                        },{
                          "datatype": "property",
                          "target": "#s-Text_16",
                          "property": "jimGetValue"
                        } ]
                      } ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimAnd",
                "parameter": [ {
                  "action": "jimGreaterOrEquals",
                  "parameter": [ {
                    "datatype": "property",
                    "target": "#s-Text_17",
                    "property": "jimGetValue"
                  },"22" ]
                },{
                  "action": "jimLessOrEquals",
                  "parameter": [ {
                    "datatype": "property",
                    "target": "#s-Text_17",
                    "property": "jimGetValue"
                  },"29" ]
                } ]
              },
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Get_it_by_2" ],
                    "value": {
                      "action": "jimConcat",
                      "parameter": [ {
                        "action": "jimConcat",
                        "parameter": [ {
                          "action": "jimPlus",
                          "parameter": [ {
                            "datatype": "property",
                            "target": "#s-Text_18",
                            "property": "jimGetValue"
                          },"1" ]
                        },"-" ]
                      },{
                        "action": "jimConcat",
                        "parameter": [ {
                          "action": "jimConcat",
                          "parameter": [ "6","-" ]
                        },{
                          "datatype": "property",
                          "target": "#s-Text_16",
                          "property": "jimGetValue"
                        } ]
                      } ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Text_17",
                  "property": "jimGetValue"
                },"30" ]
              },
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Get_it_by_2" ],
                    "value": {
                      "action": "jimConcat",
                      "parameter": [ {
                        "action": "jimConcat",
                        "parameter": [ {
                          "action": "jimPlus",
                          "parameter": [ {
                            "datatype": "property",
                            "target": "#s-Text_18",
                            "property": "jimGetValue"
                          },"1" ]
                        },"-" ]
                      },{
                        "action": "jimConcat",
                        "parameter": [ {
                          "action": "jimConcat",
                          "parameter": [ "7","-" ]
                        },{
                          "datatype": "property",
                          "target": "#s-Text_16",
                          "property": "jimGetValue"
                        } ]
                      } ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-Text_17",
                  "property": "jimGetValue"
                },"31" ]
              },
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Get_it_by_2" ],
                    "value": {
                      "action": "jimConcat",
                      "parameter": [ {
                        "action": "jimConcat",
                        "parameter": [ {
                          "action": "jimPlus",
                          "parameter": [ {
                            "datatype": "property",
                            "target": "#s-Text_18",
                            "property": "jimGetValue"
                          },"1" ]
                        },"-" ]
                      },{
                        "action": "jimConcat",
                        "parameter": [ {
                          "action": "jimConcat",
                          "parameter": [ "8","-" ]
                        },{
                          "datatype": "property",
                          "target": "#s-Text_16",
                          "property": "jimGetValue"
                        } ]
                      } ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimAnd",
                "parameter": [ {
                  "action": "jimEquals",
                  "parameter": [ {
                    "datatype": "property",
                    "target": "#s-Text_18",
                    "property": "jimGetValue"
                  },"12" ]
                },{
                  "action": "jimEquals",
                  "parameter": [ {
                    "datatype": "property",
                    "target": "#s-Text_17",
                    "property": "jimGetValue"
                  },"31" ]
                } ]
              },
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Get_it_by_2" ],
                    "value": {
                      "action": "jimConcat",
                      "parameter": [ {
                        "action": "jimConcat",
                        "parameter": [ "1","-" ]
                      },{
                        "action": "jimConcat",
                        "parameter": [ {
                          "action": "jimConcat",
                          "parameter": [ "9","-" ]
                        },{
                          "action": "jimPlus",
                          "parameter": [ {
                            "datatype": "property",
                            "target": "#s-Text_16",
                            "property": "jimGetValue"
                          },"1" ]
                        } ]
                      } ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-ButOk")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-ButOk > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-ButOk .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-ButOk span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-ButOk > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-ButOk .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-ButOk span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimDeleteData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
    jFirer.parents("tr.datarow").trigger("click", true);
  })
  .on("click", ".s-a036ca4e-2607-4d97-94f7-2339951d2b04 .toggle", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Rectangle_2")) {
      if(jFirer.data("jimHasToggle")) {
        jFirer.removeData("jimHasToggle");
        jEvent.undoCases(jFirer);
      } else {
        jFirer.data("jimHasToggle", true);
        event.backupState = true;
        event.target = jFirer;
        cases = [
          {
            "blocks": [
              {
                "actions": [
                  {
                    "action": "jimShow",
                    "parameter": {
                      "target": [ "#s-Check" ]
                    },
                    "exectype": "serial",
                    "delay": 0
                  },
                  {
                    "action": "jimShow",
                    "parameter": {
                      "target": [ "#s-ShippingGroup" ],
                      "effect": {
                        "type": "fade",
                        "easing": "linear",
                        "duration": 500
                      }
                    },
                    "exectype": "parallel",
                    "delay": 0
                  },
                  {
                    "action": "jimMove",
                    "parameter": {
                      "target": [ "#s-totalGroup" ],
                      "top": {
                        "type": "movebyoffset",
                        "value": "40"
                      },
                      "left": {
                        "type": "nomove"
                      },
                      "containment": false,
                      "effect": {
                        "type": "none",
                        "easing": "swing",
                        "duration": 500
                      }
                    },
                    "exectype": "parallel",
                    "delay": 0
                  },
                  {
                    "action": "jimSetValue",
                    "parameter": {
                      "target": [ "#s-Paragraph_7" ],
                      "value": "3.99"
                    },
                    "exectype": "parallel",
                    "delay": 0
                  }
                ]
              }
            ],
            "exectype": "serial",
            "delay": 0
          },
          {
            "blocks": [
              {
                "actions": [
                  {
                    "action": "jimSetValue",
                    "parameter": {
                      "target": [ "#s-Text_20" ],
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#s-Text_19",
                          "property": "jimGetValue"
                        },{
                          "datatype": "property",
                          "target": "#s-Paragraph_7",
                          "property": "jimGetValue"
                        } ]
                      }
                    },
                    "exectype": "serial",
                    "delay": 0
                  }
                ]
              }
            ],
            "exectype": "serial",
            "delay": 0
          },
          {
            "blocks": [
              {
                "actions": [
                  {
                    "action": "jimSetValue",
                    "parameter": {
                      "target": [ "#s-ExDeliveryDate" ],
                      "value": {
                        "datatype": "property",
                        "target": "#Get_it_by_1",
                        "property": "jimGetValue"
                      }
                    },
                    "exectype": "serial",
                    "delay": 0
                  }
                ]
              }
            ],
            "exectype": "serial",
            "delay": 0
          },
          {
            "blocks": [
              {
                "actions": [
                  {
                    "action": "jimShow",
                    "parameter": {
                      "target": [ "#s-ExDeliveryDate" ]
                    },
                    "exectype": "serial",
                    "delay": 0
                  },
                  {
                    "action": "jimHide",
                    "parameter": {
                      "target": [ "#s-regularDeliveryDate" ]
                    },
                    "exectype": "parallel",
                    "delay": 0
                  }
                ]
              }
            ],
            "exectype": "serial",
            "delay": 0
          }
        ];
        jEvent.launchCases(cases);
      }
    }
  })
  .on("pageload", ".s-a036ca4e-2607-4d97-94f7-2339951d2b04 .pageload", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Get_it_by_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Text_14" ],
                    "value": {
                      "action": "jimSubstring",
                      "parameter": [ {
                        "action": "jimSystemDate"
                      },"0","2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Text_13" ],
                    "value": {
                      "action": "jimSubstring",
                      "parameter": [ {
                        "action": "jimSystemDate"
                      },"3","5" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Text_12" ],
                    "value": {
                      "action": "jimSubstring",
                      "parameter": [ {
                        "action": "jimSystemDate"
                      },"6","10" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Panel_3")) {
      cases = [
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"3" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#s-Panel_3" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "datatype": "property",
                        "target": "#s-productList",
                        "property": "jimGetHeight"
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 300
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreaterOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#s-Panel_3" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "330"
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 300
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is(".s-Current_row_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ ".s-Image_7" ],
                    "value": {
                      "action": "jimFilterData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "datatype": "property",
                          "target": ".s-Input_11",
                          "property": "jimGetValue"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is(".s-Input_13")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ ".s-Input_13" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "datatype": "property",
                        "target": ".s-Input_13",
                        "property": "jimGetValue"
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#mi-d4c70b1d-Paragraph_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#mi-d4c70b1d-Paragraph_4" ],
                    "value": {
                      "action": "jimSubstring",
                      "parameter": [ {
                        "action": "jimSystemDate"
                      },"0","2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#mi-d4c70b1d-Paragraph_3" ],
                    "value": {
                      "action": "jimSubstring",
                      "parameter": [ {
                        "action": "jimSystemDate"
                      },"3","5" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#mi-d4c70b1d-Paragraph_2" ],
                    "value": {
                      "action": "jimSubstring",
                      "parameter": [ {
                        "action": "jimSystemDate"
                      },"6","10" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#mi-d4c70b1d-Paragraph_3",
                  "property": "jimGetValue"
                },"29" ]
              },
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#mi-d4c70b1d-Paragraph_1" ],
                    "value": {
                      "action": "jimConcat",
                      "parameter": [ {
                        "action": "jimConcat",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#mi-d4c70b1d-Paragraph_4",
                          "property": "jimGetValue"
                        },"-" ]
                      },{
                        "action": "jimConcat",
                        "parameter": [ {
                          "action": "jimConcat",
                          "parameter": [ {
                            "action": "jimPlus",
                            "parameter": [ {
                              "datatype": "property",
                              "target": "#mi-d4c70b1d-Paragraph_3",
                              "property": "jimGetValue"
                            },"2" ]
                          },"-" ]
                        },{
                          "datatype": "property",
                          "target": "#mi-d4c70b1d-Paragraph_2",
                          "property": "jimGetValue"
                        } ]
                      } ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#mi-d4c70b1d-Paragraph_3",
                  "property": "jimGetValue"
                },"30" ]
              },
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#mi-d4c70b1d-Paragraph_1" ],
                    "value": {
                      "action": "jimConcat",
                      "parameter": [ {
                        "action": "jimConcat",
                        "parameter": [ {
                          "action": "jimPlus",
                          "parameter": [ {
                            "datatype": "property",
                            "target": "#mi-d4c70b1d-Paragraph_4",
                            "property": "jimGetValue"
                          },"1" ]
                        },"-" ]
                      },{
                        "action": "jimConcat",
                        "parameter": [ {
                          "action": "jimConcat",
                          "parameter": [ "1","-" ]
                        },{
                          "datatype": "property",
                          "target": "#mi-d4c70b1d-Paragraph_2",
                          "property": "jimGetValue"
                        } ]
                      } ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#mi-d4c70b1d-Paragraph_3",
                  "property": "jimGetValue"
                },"31" ]
              },
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#mi-d4c70b1d-Paragraph_1" ],
                    "value": {
                      "action": "jimConcat",
                      "parameter": [ {
                        "action": "jimConcat",
                        "parameter": [ {
                          "action": "jimPlus",
                          "parameter": [ {
                            "datatype": "property",
                            "target": "#mi-d4c70b1d-Paragraph_4",
                            "property": "jimGetValue"
                          },"1" ]
                        },"-" ]
                      },{
                        "action": "jimConcat",
                        "parameter": [ {
                          "action": "jimConcat",
                          "parameter": [ "2","-" ]
                        },{
                          "datatype": "property",
                          "target": "#mi-d4c70b1d-Paragraph_2",
                          "property": "jimGetValue"
                        } ]
                      } ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimAnd",
                "parameter": [ {
                  "action": "jimEquals",
                  "parameter": [ {
                    "datatype": "property",
                    "target": "#mi-d4c70b1d-Paragraph_4",
                    "property": "jimGetValue"
                  },"12" ]
                },{
                  "action": "jimEquals",
                  "parameter": [ {
                    "datatype": "property",
                    "target": "#mi-d4c70b1d-Paragraph_3",
                    "property": "jimGetValue"
                  },"31" ]
                } ]
              },
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#mi-d4c70b1d-Paragraph_1" ],
                    "value": {
                      "action": "jimConcat",
                      "parameter": [ {
                        "action": "jimConcat",
                        "parameter": [ "1","-" ]
                      },{
                        "action": "jimConcat",
                        "parameter": [ {
                          "action": "jimConcat",
                          "parameter": [ "3","-" ]
                        },{
                          "action": "jimPlus",
                          "parameter": [ {
                            "datatype": "property",
                            "target": "#mi-d4c70b1d-Paragraph_2",
                            "property": "jimGetValue"
                          },"1" ]
                        } ]
                      } ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_20")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Text_19","#s-Text_20" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimPlus",
                        "parameter": [ {
                          "action": "jimSumData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts",
                            "value": {
                              "action": "jimMultiply",
                              "parameter": [ {
                                "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                              },{
                                "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                              } ]
                            }
                          }
                        },{
                          "datatype": "property",
                          "target": "#s-Paragraph_7",
                          "property": "jimGetValue"
                        } ]
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("change", ".s-a036ca4e-2607-4d97-94f7-2339951d2b04 .change", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-SelectCard")) {
      cases = [
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-SelectCard",
                  "property": "jimGetSelectedValue"
                },"Visa" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Visa" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-SelectCard",
                  "property": "jimGetSelectedValue"
                },"Master Card" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Master" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-SelectCard",
                  "property": "jimGetSelectedValue"
                },"American Express" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-American" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#s-SelectCard",
                  "property": "jimGetSelectedValue"
                },"Discovery" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#s-Discovery" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is(".s-Category_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },{
                          "datatype": "property",
                          "target": ".s-Input_11",
                          "property": "jimGetValue"
                        } ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "datatype": "property",
                        "target": ".s-Category_1",
                        "property": "jimGetSelectedValue"
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Text_19" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#s-Text_20" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimPlus",
                        "parameter": [ {
                          "action": "jimSumData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts",
                            "value": {
                              "action": "jimMultiply",
                              "parameter": [ {
                                "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                              },{
                                "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                              } ]
                            }
                          }
                        },{
                          "datatype": "property",
                          "target": "#s-Paragraph_7",
                          "property": "jimGetValue"
                        } ]
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("focusin", ".s-a036ca4e-2607-4d97-94f7-2339951d2b04 .focusin", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Input_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Text_2" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_1 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_1 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#999999",
                      "border-right-width": "1.0px",
                      "border-right-color": "#999999",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#999999",
                      "border-left-width": "1.0px",
                      "border-left-color": "#999999",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_1 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-FIRSTNAME .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-FIRSTNAME span" ],
                    "attributes": {
                      "color": "#585858",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Text_1" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_2 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_2 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#999999",
                      "border-right-width": "1.0px",
                      "border-right-color": "#999999",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#999999",
                      "border-left-width": "1.0px",
                      "border-left-color": "#999999",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_2 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-SECONDNAME .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-SECONDNAME span" ],
                    "attributes": {
                      "color": "#585858",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_3")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Text_3" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_3 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_3 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#999999",
                      "border-right-width": "1.0px",
                      "border-right-color": "#999999",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#999999",
                      "border-left-width": "1.0px",
                      "border-left-color": "#999999",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_3 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-EMAIL .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-EMAIL span" ],
                    "attributes": {
                      "color": "#585858",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_4")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Text_4" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_4 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_4 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#999999",
                      "border-right-width": "1.0px",
                      "border-right-color": "#999999",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#999999",
                      "border-left-width": "1.0px",
                      "border-left-color": "#999999",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_4 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-PHONENUMBER .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-PHONENUMBER span" ],
                    "attributes": {
                      "color": "#585858",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_5")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Text_5" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_5 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_5 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#999999",
                      "border-right-width": "1.0px",
                      "border-right-color": "#999999",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#999999",
                      "border-left-width": "1.0px",
                      "border-left-color": "#999999",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_5 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-ADRESS .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-ADRESS span" ],
                    "attributes": {
                      "color": "#585858",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_6")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Text_9" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_6 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_6 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#999999",
                      "border-right-width": "1.0px",
                      "border-right-color": "#999999",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#999999",
                      "border-left-width": "1.0px",
                      "border-left-color": "#999999",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_6 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-ZIPCODE .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-ZIPCODE span" ],
                    "attributes": {
                      "color": "#585858",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_7")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Text_10" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_7 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_7 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#999999",
                      "border-right-width": "1.0px",
                      "border-right-color": "#999999",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#999999",
                      "border-left-width": "1.0px",
                      "border-left-color": "#999999",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_7 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-STATE .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-STATE span" ],
                    "attributes": {
                      "color": "#585858",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_8")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Text_6" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_8 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_8 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#999999",
                      "border-right-width": "1.0px",
                      "border-right-color": "#999999",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#999999",
                      "border-left-width": "1.0px",
                      "border-left-color": "#999999",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_8 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-CARDNUMBER .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-CARDNUMBER span" ],
                    "attributes": {
                      "color": "#585858",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_9")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Text_7" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_9 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_9 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#999999",
                      "border-right-width": "1.0px",
                      "border-right-color": "#999999",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#999999",
                      "border-left-width": "1.0px",
                      "border-left-color": "#999999",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_9 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-NAMEON_CARD .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-NAMEON_CARD span" ],
                    "attributes": {
                      "color": "#585858",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Input_10")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Text_8" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_10 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_10 > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#999999",
                      "border-right-width": "1.0px",
                      "border-right-color": "#999999",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#999999",
                      "border-left-width": "1.0px",
                      "border-left-color": "#999999",
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-Input_10 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "0.0px",
                      "border-top-right-radius": "0.0px",
                      "border-bottom-right-radius": "0.0px",
                      "border-bottom-left-radius": "0.0px"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-CVCCODE .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-CVCCODE span" ],
                    "attributes": {
                      "color": "#585858",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-monthCard")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Text_11" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .line_options.selected" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .selected > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .line_options:hover" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-monthCard .line_options:hover > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-monthCard > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#999999",
                      "border-right-width": "1.0px",
                      "border-right-color": "#999999",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#999999",
                      "border-left-width": "1.0px",
                      "border-left-color": "#999999"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-VALIDTHROUGH span" ],
                    "attributes": {
                      "color": "#585858",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-yearCard")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#s-Text_11" ]
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-yearCard .line_options.selected" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-yearCard .selected > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-yearCard .line_options:hover" ],
                    "attributes": {
                      "background-color": "#000000 !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 .s-yearCard .line_options:hover > .options" ],
                    "attributes": {
                      "color": "#FFFFFF !important"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-yearCard > .borderLayer" ],
                    "attributes": {
                      "border-top-width": "1.0px",
                      "border-top-color": "#999999",
                      "border-right-width": "1.0px",
                      "border-right-color": "#999999",
                      "border-bottom-width": "1.0px",
                      "border-bottom-style": "solid",
                      "border-bottom-color": "#999999",
                      "border-left-width": "1.0px",
                      "border-left-color": "#999999"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-VALIDTHROUGH .valign" ],
                    "attributes": {
                      "vertical-align": "top",
                      "text-align": "left"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-VALIDTHROUGH span" ],
                    "attributes": {
                      "color": "#585858",
                      "text-align": "left",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "8.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "PurchaseValidation" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("swipeleft", ".s-a036ca4e-2607-4d97-94f7-2339951d2b04 .swipeleft", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is(".s-Row_cell_4")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ ".s-Group_1" ],
                    "top": {
                      "type": "nomove"
                    },
                    "left": {
                      "type": "movetoposition",
                      "value": "-90"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-productList .s-Row_cell_4 .verticalalign" ],
                    "attributes": {
                      "vertical-align": "top"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-productList .s-Row_cell_4 > .cellContainerChild > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#F4F4F4"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("swiperight", ".s-a036ca4e-2607-4d97-94f7-2339951d2b04 .swiperight", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is(".s-Row_cell_4")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ ".s-Group_1" ],
                    "top": {
                      "type": "nomove"
                    },
                    "left": {
                      "type": "movetoposition",
                      "value": "0"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-productList .s-Row_cell_4 .verticalalign" ],
                    "attributes": {
                      "vertical-align": "top"
                    }
                  },{
                    "target": [ "#s-a036ca4e-2607-4d97-94f7-2339951d2b04 #s-productList .s-Row_cell_4 > .cellContainerChild > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  });