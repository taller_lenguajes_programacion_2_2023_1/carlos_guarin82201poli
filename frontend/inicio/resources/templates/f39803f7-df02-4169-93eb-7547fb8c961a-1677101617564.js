jQuery("#simulation")
  .on("click", ".t-f39803f7-df02-4169-93eb-7547fb8c961a .click", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#t-Image_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/7ab51cc7-2f32-40b2-afc2-c986938d6ed6"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#t-Hotspot_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productList" ],
                    "value": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#t-f39803f7-df02-4169-93eb-7547fb8c961a #t-Image_13 svg" ],
                    "attributes": {
                      "overlay": "#7D5FFF"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimMultiply",
                        "parameter": [ {
                          "action": "jimCountData",
                          "parameter": {
                            "datatype": "datamaster",
                            "datamaster": "OrderProducts"
                          }
                        },"101" ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimGreater",
                "parameter": [ {
                  "action": "jimCount",
                  "parameter": [ {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  } ]
                },"4" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": "404"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#t-xClose_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-sCart" ],
                    "effect": {
                      "type": "slide",
                      "easing": "swing",
                      "duration": 500,
                      "direction": "up"
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimHide",
                  "parameter": {
                    "target": [ "#t-Rectangle_1" ],
                    "effect": {
                      "type": "fade",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#t-f39803f7-df02-4169-93eb-7547fb8c961a #t-Image_13 svg" ],
                    "attributes": {
                      "overlay": "#434343"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "ShowHideCart" ],
                    "value": "0"
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is(".t-Category_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": [ "ShowHideCart" ],
                    "value": "1"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is(".t-Image_11")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimDeleteData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },null ]
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimLessOrEquals",
                "parameter": [ {
                  "action": "jimCountData",
                  "parameter": {
                    "datatype": "datamaster",
                    "datamaster": "OrderProducts"
                  }
                },"3" ]
              },
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-Container" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": null
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 300
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "parallel",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimResize",
                  "parameter": {
                    "target": [ "#t-sCartList" ],
                    "width": {
                      "type": "noresize"
                    },
                    "height": {
                      "type": "exprvalue",
                      "value": {
                        "action": "jimPlus",
                        "parameter": [ {
                          "datatype": "property",
                          "target": "#t-Button_6",
                          "property": "jimGetPositionY"
                        },"63" ]
                      }
                    },
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#t-Button_6")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#t-f39803f7-df02-4169-93eb-7547fb8c961a #t-Button_6 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #7D5FFF 0.0%, #A378FF 100.0%)"
                    }
                  },{
                    "target": [ "#t-f39803f7-df02-4169-93eb-7547fb8c961a #t-Button_6 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#t-f39803f7-df02-4169-93eb-7547fb8c961a #t-Button_6 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "9.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "17px"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#t-f39803f7-df02-4169-93eb-7547fb8c961a #t-Button_6 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent",
                      "background-image#draft": "linear-gradient(0.0% 50.0% to 100.0% 50.0%, #A378FF 0.0%, #7D5FFF 100.0%)"
                    }
                  },{
                    "target": [ "#t-f39803f7-df02-4169-93eb-7547fb8c961a #t-Button_6 .valign" ],
                    "attributes": {
                      "vertical-align": "middle",
                      "text-align": "center"
                    }
                  },{
                    "target": [ "#t-f39803f7-df02-4169-93eb-7547fb8c961a #t-Button_6 span" ],
                    "attributes": {
                      "color": "#FFFFFF",
                      "text-align": "center",
                      "text-decoration": "none",
                      "font-family": "'Montserrat-Regular_wgt400',Arial",
                      "font-size": "10.0pt",
                      "font-weight": "400",
                      "font-stretch": "normal",
                      "line-height": "19px"
                    }
                  } ],
                  "exectype": "timed",
                  "delay": 300
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/a036ca4e-2607-4d97-94f7-2339951d2b04",
                    "transition": {
                      "type": "fade",
                      "duration": 700
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
    jFirer.parents("tr.datarow").trigger("click", true);
  })
  .on("pageload", ".t-f39803f7-df02-4169-93eb-7547fb8c961a .pageload", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#t-productsCounter")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "condition": {
                "action": "jimNotEquals",
                "parameter": [ {
                  "datatype": "property",
                  "target": "#t-productsCounter",
                  "property": "jimGetValue"
                },"0" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "effect": {
                      "type": "puff",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is(".t-Current_row_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ ".t-Image_7" ],
                    "value": {
                      "action": "jimFilterData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "datatype": "property",
                          "target": ".t-Input_11",
                          "property": "jimGetValue"
                        }
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is(".t-Input_13")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ ".t-Input_13" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "datatype": "property",
                        "target": ".t-Input_13",
                        "property": "jimGetValue"
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("change", ".t-f39803f7-df02-4169-93eb-7547fb8c961a .change", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is(".t-Category_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimUpdateData",
                  "parameter": {
                    "action": "jimFilterData",
                    "parameter": {
                      "datatype": "datamaster",
                      "datamaster": "OrderProducts",
                      "value": {
                        "action": "jimEquals",
                        "parameter": [ {
                          "field": "67794fbb-79bb-47e2-b2d9-95c273dc6789"
                        },null ]
                      }
                    },
                    "fields": {
                      "7c564f00-5b04-4551-9c7e-cb6333e65e0d": null,
                      "67794fbb-79bb-47e2-b2d9-95c273dc6789": null,
                      "1836c6e8-9e80-49e8-b116-4435848722bf": null,
                      "e1029159-a43a-42b8-af4c-c04ee4c4dd4d": null,
                      "a84ba135-2ff9-4f56-85d1-c15434e35673": {
                        "datatype": "property",
                        "target": ".t-Category_1",
                        "property": "jimGetSelectedValue"
                      }
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-Text_2" ],
                    "value": {
                      "action": "jimRound",
                      "parameter": [ {
                        "action": "jimSumData",
                        "parameter": {
                          "datatype": "datamaster",
                          "datamaster": "OrderProducts",
                          "value": {
                            "action": "jimMultiply",
                            "parameter": [ {
                              "field": "e1029159-a43a-42b8-af4c-c04ee4c4dd4d"
                            },{
                              "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                            } ]
                          }
                        }
                      },"2" ]
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "target": [ "#t-productsCounter" ],
                    "value": {
                      "action": "jimSumData",
                      "parameter": {
                        "datatype": "datamaster",
                        "datamaster": "OrderProducts",
                        "value": {
                          "field": "a84ba135-2ff9-4f56-85d1-c15434e35673"
                        }
                      }
                    }
                  },
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("swipeleft", ".t-f39803f7-df02-4169-93eb-7547fb8c961a .swipeleft", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is(".t-Row_cell_4")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ ".t-Group_1" ],
                    "top": {
                      "type": "nomove"
                    },
                    "left": {
                      "type": "movetoposition",
                      "value": "-100"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#t-f39803f7-df02-4169-93eb-7547fb8c961a #t-productList .t-Row_cell_4 .verticalalign" ],
                    "attributes": {
                      "vertical-align": "top"
                    }
                  },{
                    "target": [ "#t-f39803f7-df02-4169-93eb-7547fb8c961a #t-productList .t-Row_cell_4 > .cellContainerChild > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#F4F4F4"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("swiperight", ".t-f39803f7-df02-4169-93eb-7547fb8c961a .swiperight", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is(".t-Row_cell_4")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimMove",
                  "parameter": {
                    "target": [ ".t-Group_1" ],
                    "top": {
                      "type": "nomove"
                    },
                    "left": {
                      "type": "movetoposition",
                      "value": "0"
                    },
                    "containment": false,
                    "effect": {
                      "type": "none",
                      "easing": "swing",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#t-f39803f7-df02-4169-93eb-7547fb8c961a #t-productList .t-Row_cell_4 .verticalalign" ],
                    "attributes": {
                      "vertical-align": "top"
                    }
                  },{
                    "target": [ "#t-f39803f7-df02-4169-93eb-7547fb8c961a #t-productList .t-Row_cell_4 > .cellContainerChild > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "transparent"
                    }
                  } ],
                  "exectype": "parallel",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  });