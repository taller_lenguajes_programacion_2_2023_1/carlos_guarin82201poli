(function(window, undefined) {
  var dictionary = {
    "a3abeb3c-4193-47b4-b708-428758bd196a": "Detalle 2",
    "567e87e5-d40a-4587-9d00-43a32be09394": "Registro",
    "14076d24-bea1-496b-a28b-dd4f95f3146b": "Detalle 3",
    "390b9a82-14f7-4a7e-b714-3158b656b9f6": "Detalle 1",
    "eca691a4-8a71-41ad-ae23-f45b3cc79c74": "Inicio+ Dropdowns",
    "a036ca4e-2607-4d97-94f7-2339951d2b04": "Carrito",
    "d12245cc-1680-458d-89dd-4f0d7fb22724": "Inicio",
    "685c25f0-2f46-4dff-a050-86f931de0df8": "Consulta",
    "7ab51cc7-2f32-40b2-afc2-c986938d6ed6": "Login",
    "662ab3f7-67d4-4a0d-a75d-a99b24e3ed90": "plantilla",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "36ea9244-cf66-4f1d-844f-ab9786cccbfb": "960 grid - 16 columns",
    "4d52413a-bd9b-45e8-949f-ebc75f0e525e": "960 grid - 12 columns",
    "b6c9522a-9815-4308-9d10-0fc53d52c801": "ProductInfo",
    "18dfdd79-569e-4803-a627-e7377c729229": "GetItByDate",
    "1253e5e9-1afa-4cff-b08a-f2462ce0321d": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);